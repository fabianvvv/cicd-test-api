
dotnet-build-api:
	dotnet publish src/Server/fabsi.TestApi/fabsi.TestApi.csproj -c Release --output src/Server/out

docker-build-api:
	docker build . -t weatherforecast-api-image -f Dockerfile.server --build-arg DOTNET_VERSION=3.1

docker-run-api:
	docker run -d -p 5000:80 -p 5001:442 --name weatherforecast-api weatherforecast-api-image

docker-stop-api:
	docker stop weatherforecast-api

docker-remove-api:
	docker rm weatherforecast-api

docker-kill-api:
	docker stop weatherforecast-api
	docker rm weatherforecast-api

docker-remove-image:
	docker image rm weatherforecast-api-image

curl-get-api:
	curl -X GET http://localhost:5000/weatherforecast
	curl -X GET http://localhost:5001/weatherforecast
